e3-mpsplc
===========

Introduction  
===========
**ESS Site-specific EPICS module** mpsplc

**Physical processes**

The MPS IOCs monitor the machine protection PLCs for magnets (power converters for quadrupoles), vacuum (sector gate valves) and insertable devices (FCs, WSAs, EMUSs,EMUAs,COLLAs,BSMA) for the Normal Conducting LINAC. A total of 11 power converters, 8 VVSs and 20 IDs.

**Components used**

The MPS PLCs implement the Protection Function control logic. The IOCs monitor the PLCs data: diagnostics, FBIS interface, position switches of VVS and IDs, and interlock signals based on current loops coming from the local protection systems (power converters electronics, interlock PLC for Vacuum controls and cooling of IDs).

**Main functions**
- Provide the Beam Mode and Beam Destination to the MPS PLCs for use in Protection Function logic
- Allow the monitoring of the PLC data described above and show to the operators and system experts information related to potential interlocks generated by PLC based machine protection systems

External services required
=====================
- Alarms handler
- Archiver
- CCDB
- OPIs design
- VM and gateway machines

Interfaces to other systems
======================
- Timing IOC to provide to the MPS PLCs the Beam Configuration (intended Beam Mode and Beam Destination)
- MPS PLCs: MPSVac, MPSMag and MPSID through dedicated gateway CPUs
- Networking, specialities if any (like separate VLAN for xxxx): our IOCs are currently running in the TN:
- VM (soft IOCs) called “mpsid-ipc-02.tn.esss.lu.se” 
- cagateway: “cagw-mps.tn.esss.lu.se”
- VLAN called “ChannelAccess-PS” where our MPS PLCs, the VM that runs the IOCs and the cagateway are connected 
- [Detailed networking info](https://confluence.esss.lu.se/pages/viewpage.action?pageId=339006207)

Organisation of Template Files
===================
Most of the template files are shared by the three IOCs. The ones specific for each IOC start with a description that identify them easily like:
- MPSVac: substitution file used "mspvac-.....". Dedicated templates: "vvs_...."
- MPSID: substitution file used "mpsid-....". Dedicated templates: "ids_..."
- MPSMag: substitution file used "mpsmag-......". Dedicated templates: "pwrs_....."

Use of MACROS
=============
In the **startup scripts**, the PREFIX, REC, DDI,.... are instanciated as (taking as an example the MPSID IOC):
_dbLoadTemplate("$(TOP)/mpsplc-loc/mpsplcApp/Db/mpsid-plc-ess.substitutions", "PREFIX=MPSID:,SUBSCRIPT=$(SUBSCRIPT-MPSID),DDI=:,DDI2=Ctrl-FCPU-01:,REC=Ctrl-IOC-01:,IOCNAME=$(IOCNAME)")_
In the **substitution files** for each IOC, with this instruction:
_global {SUBS=$(SUBSCRIPT), P=$(PREFIX), R=$(REC), R1=$(DDI), R2=$(DDI2)}_
$P,$R and so on, are initialised and used in the templates corresponding to each IOC.


GUIs
======
[Repository](https://gitlab.esss.lu.se/opis/dev-mps/systemexpert)


"e3" environment
=========
- [e3 training](https://gitlab.esss.lu.se/e3/e3training)
- [e3 installation](https://gitlab.esss.lu.se/e3/e3training/-/blob/master/workbook/chapter01.md)
- [e3 module installation](https://gitlab.esss.lu.se/e3/e3training/-/blob/master/workbook/chapter03.md)

Requirements
===========
- opcua (v0.8.0 or later)
- iocstats

Building & installation
========================
- For the mpsplc module: 'make build', 'make cellinstall' 
- To inflate databases: 'make db' 

Run the IOCs
===============
- Each MPS IOC independently: 'iocsh.bash -l cellMods cmds/st_MPSXXX.cmd' where XXX=ID,Mag or Vac
- Using snippets: 'iocsh.bash cmds/st.cmd'
