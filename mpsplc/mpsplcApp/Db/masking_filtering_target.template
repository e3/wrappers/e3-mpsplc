#From EPICS to MPSTrg Mask confirmation command
record(bo, "$(P)::C_MaskConfirm")
{
    field(DTYP, "OPCUA")
    field(OUT,  "@$(_SUBS) ns=3;s=\"dbMaskingTable\".\"EPICS_MPS$(SYS):MaskConfirm_Req\" register=y bini=write monitor=n")
    field(ONAM, "MaskConfirm MPS$(SYS)")
    field(HIGH, "2")
    field(DESC, "Masking confirmation")

    info(aa_policy, "default") 
    info(archiver, "tn")
}

#From EPICS to MPSTrg Filter confirmation command
record(bo, "$(P)::C_FilterConfirm")
{
    field(DTYP, "OPCUA")
    field(OUT,  "@$(_SUBS) ns=3;s=\"dbMaskingTable\".\"EPICS_MPS$(SYS):FilterConfirm_Req\" register=y bini=write monitor=n")
    field(ONAM, "FilterConfirm MPS$(SYS)")
    field(HIGH, "2")
    field(DESC, "Filtering confirmation")

    info(aa_policy, "default") 
    info(archiver, "tn")
}

#From MPSTrg to EPICS Masking Table ok
record(bi, "$(P)::MaskTableOk")
{
    field(DTYP, "OPCUA")
    field(INP,  "@$(_SUBS) ns=3;s=\"dbMaskingTable\".\"MPS$(SYS)_EPICS:MaskTableOk\" register=y")
    field(SCAN, "I/O Intr")
    field(TSE, "-2")
    field(ONAM, "MaskTbl OK")
    field(ZNAM, "MaskTbl NOK")
    field(DESC, "Masking table is ok")

    info(aa_policy, "default") 
    info(archiver, "tn")
}

#From MPSTrg to EPICS Filter Table ok
record(bi, "$(P)::FilterTableOk")
{
    field(DTYP, "OPCUA")
    field(INP,  "@$(_SUBS) ns=3;s=\"dbMaskingTable\".\"MPS$(SYS)_EPICS:FilterTableOk\" register=y")
    field(SCAN, "I/O Intr")
    field(TSE, "-2")
    field(ONAM, "FilterTbl OK")
    field(ZNAM, "FilterTbl NOK")
    field(DESC, "Filtering table is ok")

    info(aa_policy, "default") 
    info(archiver, "tn")
}

#From MPSTrg to EPICS AnyMaskEnbld HCS 
record(bi, "$(P)::HCS_AnyMaskEnbld")
{
    field(DTYP, "OPCUA")
    field(INP,  "@$(_SUBS) ns=3;s=\"dbMaskingTable\".\"MPS$(SYS)_EPICS:HCS_AnyMaskEnbld\" register=y")
    field(SCAN, "I/O Intr")
    field(TSE, "-2")
    field(ONAM, "Mask enabled")
    field(ZNAM, "No mask")
    field(DESC, "Masking enabled in HCS")

    info(aa_policy, "default") 
    info(archiver, "tn")
}

#From MPSTrg to EPICS AnyMaskEnbld SPWCS 
record(bi, "$(P)::SPWCS_AnyMaskEnbld")
{
    field(DTYP, "OPCUA")
    field(INP,  "@$(_SUBS) ns=3;s=\"dbMaskingTable\".\"MPS$(SYS)_EPICS:SPWCS_AnyMaskEnbld\" register=y")
    field(SCAN, "I/O Intr")
    field(TSE, "-2")
    field(ONAM, "Mask enabled")
    field(ZNAM, "No mask")
    field(DESC, "Masking enabled in SPWCS")

    info(aa_policy, "default") 
    info(archiver, "tn")
}

#From MPSTrg to EPICS AnyMaskEnbld MRWCS 
record(bi, "$(P)::MRWCS_AnyMaskEnbld")
{
    field(DTYP, "OPCUA")
    field(INP,  "@$(_SUBS) ns=3;s=\"dbMaskingTable\".\"MPS$(SYS)_EPICS:MRWCS_AnyMaskEnbld\" register=y")
    field(SCAN, "I/O Intr")
    field(TSE, "-2")
    field(ONAM, "Mask enabled")
    field(ZNAM, "No mask")
    field(DESC, "Masking enabled in MRWCS")

    info(aa_policy, "default") 
    info(archiver, "tn")
}

#From MPSTrg to EPICS AnyMaskEnbld CMS
record(bi, "$(P)::CMS_AnyMaskEnbld")
{
    field(DTYP, "OPCUA")
    field(INP,  "@$(_SUBS) ns=3;s=\"dbMaskingTable\".\"MPS$(SYS)_EPICS:CMS_AnyMaskEnbld\" register=y")
    field(SCAN, "I/O Intr")
    field(TSE, "-2")
    field(ONAM, "Mask enabled")
    field(ZNAM, "No mask")
    field(DESC, "Masking enabled in CMS")

    info(aa_policy, "default") 
    info(archiver, "tn")
}

#From MPSTrg to EPICS AnyMaskEnbld TVS
record(bi, "$(P)::TVS_AnyMaskEnbld")
{
    field(DTYP, "OPCUA")
    field(INP,  "@$(_SUBS) ns=3;s=\"dbMaskingTable\".\"MPS$(SYS)_EPICS:TVS_AnyMaskEnbld\" register=y")
    field(SCAN, "I/O Intr")
    field(TSE, "-2")
    field(ONAM, "Mask enabled")
    field(ZNAM, "No mask")
    field(DESC, "Masking enabled in TVS")

    info(aa_policy, "default") 
    info(archiver, "tn")
}

#From MPSTrg to EPICS AnyMaskEnbld TWS
record(bi, "$(P)::TWS_AnyMaskEnbld")
{
    field(DTYP, "OPCUA")
    field(INP,  "@$(_SUBS) ns=3;s=\"dbMaskingTable\".\"MPS$(SYS)_EPICS:TWS_AnyMaskEnbld\" register=y")
    field(SCAN, "I/O Intr")
    field(TSE, "-2")
    field(ONAM, "Mask enabled")
    field(ZNAM, "No mask")
    field(DESC, "Masking enabled in TWS")

    info(aa_policy, "default") 
    info(archiver, "tn")
}

#From MPSTrg to EPICS AnyMaskEnbld 
record(bi, "$(P)::AnyMaskEnbld")
{
    field(DTYP, "OPCUA")
    field(INP,  "@$(_SUBS) ns=3;s=\"dbMaskingTable\".\"MPS$(SYS)_EPICS:AnyMaskEnbld\" register=y")
    field(SCAN, "I/O Intr")
    field(TSE, "-2")
    field(ONAM, "Mask enabled")
    field(ZNAM, "No mask")
    field(DESC, "Masking enabled")

    info(aa_policy, "default") 
    info(archiver, "tn")
}
